import 'package:flutter/material.dart';
import 'package:test_swift/screens/impl/number_input.dart';

class NavigationHelper extends StatelessWidget {
  final Function send;
  final first;
  final second;
  NavigationHelper(this.send, this.first, this.second);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Your first number: $first, your second number $second"),
            Align(
              alignment: Alignment.center,
              child: RaisedButton(
                child: Text('GO'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => NumberInput(send: this.send)),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
