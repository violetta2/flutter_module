import 'dart:math';

/*
 * Generates a random number from 5
*/
class NumberGenerator {
  static int generate() {
    return (new Random().nextInt(5) + 1);
  }
}
