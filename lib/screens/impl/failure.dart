import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../main.dart';

class Failure extends StatelessWidget {
  final right;
  final wrong;
  final send;

  Failure({this.right, this.wrong, this.send});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        child: Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                Icons.error_outline_rounded,
                color: Colors.white,
                size: 60.0,
              ),
              Text(
                'Error',
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 50,
                    color: Colors.white),
              ),
              Text(
                'I thought of $right, not of $wrong',
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()),
                  );
                  send(wrong);
                },
                child: const Text('Go back', style: TextStyle(fontSize: 20)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
