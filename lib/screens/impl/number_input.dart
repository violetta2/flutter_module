import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_swift/number_generator.dart';
import '../screens.dart';

class NumberInput extends StatefulWidget {
  @override
  const NumberInput({
    Key key,
    this.send,
  }) : super(key: key);

  final Function send;
  _NumberInputState createState() => _NumberInputState();
}

class _NumberInputState extends State<NumberInput> {
  final _formKey = GlobalKey<FormState>();
  int _nb;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: Align(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(40.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("Try to guess the number that I'm thinking of..."),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Number between 1 and 5',
                  ),
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  validator: (value) {
                    print(value);
                    if (num.tryParse(value) < 1 || num.tryParse(value) > 5) {
                      return 'The number must be between 1 and 5';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      _nb = num.tryParse(value);
                    });
                  },
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      int nb = NumberGenerator.generate();
                      if (_nb == nb) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Success(right: nb, send: widget.send)),
                        );
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Failure(
                                  right: nb, wrong: _nb, send: widget.send)),
                        );
                      }
                    }
                  },
                  child: Text('Submit'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
