import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'navigation_helper.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: MaterialApp(
        home: MyApp(),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const platform = const MethodChannel('in.androidgeek.afe/data');
  _MyAppState() {
    platform.setMethodCallHandler(_receiveFromHost);
  }

  int _result = 0;
  int _first = 0;
  int _second = 0;

  Future<void> _receiveFromHost(MethodCall call) async {
    int f = 0;
    int s = 0;
    try {
      print(call.method);
      if (call.method == "fromHostToClient") {
        final String data = call.arguments;
        print(call.arguments);
        final jData = jsonDecode(data);
        f = jData['first'];
        s = jData['second'];
      }
    } on PlatformException catch (e) {
      print(e);
    }
    setState(() {
      _first = f;
      _second = s;
    });
  }

  void sendResultsToAndroidiOS(int res) {
    _result = _first;
    Map<String, dynamic> resultMap = Map();
    resultMap['operation'] = 'true';
    resultMap['result'] = res;

    _MyAppState.platform.invokeMethod("FromClientToHost", resultMap);
  }

  @override
  Widget build(BuildContext context) {
    return NavigationHelper(sendResultsToAndroidiOS, _first, _second);
  }
}
